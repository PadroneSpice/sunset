int[] bgColor = {247, 28, 255};
int[] sunColor = {255, 247, 28};
int[] horizonColor = {36, 255, 28};

//default copies
int[] defaultBgColor = {247, 28, 255};
int[] defaultSunColor = {255, 247, 28};
int[] defaultHorizonColor = {36, 255, 28};

float[] sunStat = {600,40,150,150};

void setup()
{
  size(900,400); 
}

void draw()
{
  background(bgColor[0], bgColor[1], bgColor[2]);
  noStroke();
  reset();
  
  fill(sunColor[0],sunColor[1],sunColor[2]);
  moveSun();
  ellipse(sunStat[0], sunStat[1],sunStat[2],sunStat[3]);
  fill(horizonColor[0], horizonColor[1], horizonColor[2]);
  rect(0,200, 900, 200);
}

void moveSun()
{
  sunStat[1] += .99;
  
  if (true)
  {
    System.out.println("sunStat1 is: " + sunStat[1]);
    for (int i=0; i<horizonColor.length; i++)
    {
      if (horizonColor[i] > 80)
      {
        horizonColor[i] *= .999999;
        horizonColor[0] *= .999;
      }
    }
    for (int i=0; i<bgColor.length; i++)
    {
      if (sunStat[1] >= 100 && bgColor[i] > 80)
      {
       bgColor[i] *= .999999;
      }
    }
  }
}

void reset()
{
   if (sunStat[1] >= 400)
   {
     for (int i=0; i<3; i++)
     {
       bgColor[i] = defaultBgColor[i];
       sunColor[i] = defaultSunColor[i];
       horizonColor[i] = defaultHorizonColor[i];
     }
     sunStat[1] = 40;
   }
}